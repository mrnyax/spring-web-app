# Spring Boot Web Application

Spring boot based web application

### TECHNOLOGIES

- Spring boot
- Java 11
- H2 database

### PRE REQUISITES RUNNING THE PROJECT
Before running the application, ensure the following have been set up:

1. Java version 11. Check version using command - `java -version`
2. Maven 3.6.3. Check version using command - `mvn -v`

### RUNNING THE PROJECT
From the root folder, run the following command

> mvn spring-boot:run
